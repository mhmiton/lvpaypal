<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// https://uaewebsitedevelopment.com/how-to-integrate-paypal-payment-gateway-in-laravel/
// https://www.fakeaddressgenerator.com/Random_Address/US_Alabama
// Vault - http://www.callstack.in/tech/blog/paypal-credit-card-vault-to-store-credit-card-details-on-paypal-side-3

Route::get('/', 'PaymentApi\PaypalCn@index')->name('/');
Route::post('/paypal', 'PaymentApi\PaypalCn@paypal')->name('paypal');
Route::any('/paypal-success', 'PaymentApi\PaypalCn@success')->name('paypal.success');
Route::any('/paypal-cancel', 'PaymentApi\PaypalCn@cancel')->name('paypal.cancel');
Route::get('/paypal-transaction/{id?}', 'PaymentApi\PaypalCn@transaction')->name('paypal.transaction');
Route::get('/paypal-vault/', 'PaymentApi\PaypalCn@vault')->name('paypal.vault');
Route::get('/paypal-vault-save', 'PaymentApi\PaypalCn@vaultSave')->name('paypal.vault.save');
Route::get('/paypal-vault-list/{id?}', 'PaymentApi\PaypalCn@vaultList')->name('paypal.vaultList');




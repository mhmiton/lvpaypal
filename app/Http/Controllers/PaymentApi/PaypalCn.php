<?php

namespace App\Http\Controllers\PaymentApi;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\FundingInstrument;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaypalCn extends Controller
{
    private $apiContext;

    public function __construct($foo = null)
    {
        $paypalConf = config()->get('paypal');

        $this->apiContext = new ApiContext(new OAuthTokenCredential(
            $paypalConf['client_id'],
            $paypalConf['secret']
        ));

        $this->apiContext->setConfig($paypalConf['settings']);
    }

    public function index()
    {
        return view('checkout');
    }


    public function paypal(Request $request)
    {
        $card = new CreditCard();
        $card->setNumber('4032032476376683');
        $card->setType('visa');
        $card->setExpireMonth(2);
        $card->setExpireYear(2023);
        $card->setCvv2('123');
        $card->setFirstName('Farhan');
        $card->setLastName('Khan');
        
        $fundinginstrument = new FundingInstrument();
        $fundinginstrument->setCreditCard($card);

        $payer = new Payer();
        // $payer->setPaymentMethod("paypal");

        $payer->setPaymentMethod('credit_card');
        $payer->setFundingInstruments(array($fundinginstrument));


        $item_1 = new Item();
        $item_1->setName('Item 1')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($request->amount);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal($request->amount);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('paypal.success'))->setCancelUrl(route('paypal.cancel'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $result = $payment->create($this->apiContext);

            if ($payer->getPaymentMethod() == 'credit_card') {
                $redirect_url = route('paypal.success', ['paymentId' => $payment->getId()]);;
                return redirect()->away($redirect_url);
            }

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            dd($ex);
            // session()->flash('msg', ['type' => 'error', 'text' => 'Connection timeout']);
            session()->flash('msg', ['type' => 'error', 'text' => 'Some error occur, sorry for inconvenient']);
            return redirect()->route('/');
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return redirect()->away($redirect_url);
        }

        session()->flash('msg', ['type' => 'error', 'text' => 'Unknown error occurred']);
        return redirect()->route('/');
    }

    public function success()
    {        
        try {
            $payment = Payment::get(request()->input('paymentId'), $this->apiContext);

            if (request()->input('PayerID')) {
                $execution = new PaymentExecution();
                $execution->setPayerId(request()->input('PayerID'));
                $payment = $payment->execute($execution, $this->apiContext);
            }
            
            session()->flash('msg', ['type' => 'success', 'text' => 'Payment successfull']);
            return redirect()->route('/');
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            dd($ex);
        }
    }

    public function cancel()
    {
        session()->flash('msg', ['type' => 'error', 'text' => 'Payment canceled']);
        return redirect()->route('/');
    }

    public function transaction($id = null)
    {
        if ($id) {
            try {
                $payment = Payment::get($id, $this->apiContext);
                dd($payment);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                dd($ex);
            }
        } else {
            try {
                $params = array('count' => 10, 'start_index' => 0);
                $payments = Payment::all($params, $this->apiContext);
                dd($payments);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                dd($ex);
            }
        }
    }

    public function vault()
    {
        $creditCardToken = new CreditCardToken();
        $creditCardToken->setCreditCardId('CARD-9H820510CB0976713LVTXGBQ');

        $fundinginstrument = new FundingInstrument();
        $fundinginstrument->setCreditCardToken($creditCardToken);

        $payer = new Payer();
        $payer->setPaymentMethod('credit_card');
        $payer->setFundingInstruments(array($fundinginstrument));

        $item_1 = new Item();
        $item_1->setName('Item 1')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice(20);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal(20);


        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setItemList($item_list);
        $transaction->setDescription("creating a payment with saved credit card");

        $payment = new Payment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setTransactions(array($transaction));

        try {
            $result = $payment->create($this->apiContext);
            dd($result);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            dd($ex);
        }
    }

    public function vaultSave()
    {
        $card = new CreditCard();
        $card->setNumber('4032032476376683');
        $card->setType('visa');
        $card->setExpireMonth(2);
        $card->setExpireYear(2023);
        $card->setCvv2('123');
        $card->setFirstName('Farhan');
        $card->setLastName('Khan');

        try {
            $result = $card->create($this->apiContext);
            dd($result);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            dd($ex);
        }
    }

    public function vaultList($id = null)
    {
        $card = new CreditCard();
        if ($id) {
            try {
                $result = $card->get($id, $this->apiContext);
                dd($result);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                dd($ex);
            }
        } else {
            try {
                $params = array('count' => 10, 'start_index' => 0);
                $result = $card->all($params, $this->apiContext);
                dd($result);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                dd($ex);
            }
        }
    }
}
